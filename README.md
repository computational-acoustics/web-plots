# Web Plots

Julia functions to produce HTML plots for the [Computational Acoustics with Open Source Software website](https://computational-acoustics.gitlab.io/website/) ([repository](https://gitlab.com/computational-acoustics/website))